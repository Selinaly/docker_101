create database mysimplesql;

create table user(
    id int not null primary key,
    firstname varchar(30),
    lastname varchar(30),
    email varchar(30),
    age INT(3),
    location varchar(50),
    date TIMESTAMP
);