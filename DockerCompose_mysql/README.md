# Building simple web and mySQL with docker compose

This repo will build from teo docker files in root to create a working docker compose.

Please read on:

## Pre Requisits:
- Docker

## Objective 

Have a super simple httpd page that kists the existing tables on a db.
For this we'll need:
- httpd container
- mysql container 
  - Create a table 
  - Give it a password and other environment variables 
  - Find a way of getting the data into a text file 
