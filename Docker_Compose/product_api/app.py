from flask import Flask
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

class Product(Resource):
    def get(self):
        return {
            'products': ['Blueberry Muffin', 'Chocolate Muffin', 'Cheese Muffin']
        }

api.add_resource(Product, '/')

class Productprice(Resource):
    def get(self):
        return {
            'products': {'Blueberry Muffin': 10, 
            'Chocolate Muffin': 5, 
            'Cheese Muffin': 15}
        }

api.add_resource(Productprice, '/price')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
