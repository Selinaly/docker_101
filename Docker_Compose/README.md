# Vegan Start Muffins :star: with Docker

We'll explore docker compose with a simple Flask API and PHP app containers.


## Step 1 - create simple API with python & containarise

Where you have the docker file, run: 
`docker build -t api_muffins .`


## Step 2 - Create simple php app that parse a JSON 

This one has not dockerfile.

## Step 3 - build docker compose file

This will have two main services: 

```
version: "3.0"

services: 
    product-api:
    web-server:

```

First service, product-api we build from docker file. We also add a volume and a ports to make it usable as a development environment.

```
version: "3.0"

services: 
    product-api:
        #builds new image
        build: ./product_api
        volumes: 
            - ./product_api:/usr/src/app
        ports: 
            - 5011:80

    web-server:

```

#### Dev instructions 

1) To run a local server run docker compose
2) check you app in localhost:5011
3) Run local test with `xyz command`


Then build the php front end serice: 

```
version: "3.0"

services: 
    product-api:
        #(...)

    web-server:
        #no docker file, build from image
        image: php:apache
        volumes: 
            - ./php_front_end:/var/www/html
        ports: 
            - 5000:80
        depends_on: 
            - product-api

```