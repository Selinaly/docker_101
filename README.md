# Docker 101

what is docker? - docker is a container tool that helps us build containers.

## Pre requisits

- Install docker
- Have it working on the CLI

## Main commands

Running our first container

```bash
docker run -dp 80:80 docker/getting-started

#docker run (-options) owner-repo/image
    # -d detached /daemon (run in background)
    # -p port - specify a mapping of port (80:80 hosts:guest)
        # The host is the physical machine you are running docker on
        # the guest is an app inside the container

docker ps
```

Let's change the port to 7900

```
docker run -dp 7900:80 docker/getting-started
```

Remove docker on port 80 

```
docker ps
--> Container ID (...)
--> d1793a3f1a77
--> df999f76eb28 

docker kill <Container ID> 
docker kill df999f76eb28 
 
docker ps
--> Container ID (...)
--> d1793a3f1a77
```
Naming containers

```
docker run --name my_apache httpd
```

--> Names of containers must be unique. (docker_container + image)

Delete container by name

```
docker ps -a
--> LIST of all running containers

docker rm <container name>
```
## Run with name and detached and getting information to terminal 

Semi debugging mode
Run container with `-dit`

where `d` detached 
where `t` terminal

--> Creates logs

## Some possible log locations on linux:

- var/log/messages
- 
## Docker Hub

What is docker hub?
find hello world docker image
download and have it running on port 80
how do you give a running container a name?
- Run another conatiner with hello world called bazinga and run it on port 80