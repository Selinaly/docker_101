FROM centos:centos8.3.2011
RUN yum -y install httpd
COPY index.html var/www/html
EXPOSE 80
EXPOSE 443
ENTRYPOINT ["httpd", "-DFOREGROUND"]